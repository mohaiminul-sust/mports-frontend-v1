import qs from 'qs'

const ARTICLE_LIST_RELATIONS = ['tags', 'banner', 'author']
const ARTICLE_DETAIL_RELATIONS = [
  'tags',
  'banner',
  'category',
  'author.avatar',
  'content.photo',
  'content.video',
  'content.photos',
]

export const getArticlesQuery = (page, pageSize) => {
  return qs.stringify(
    {
      fields: ['title', 'short_desc', 'publish_date', 'slug'],
      pagination: {
        pageSize: pageSize,
        page: page,
      },
      publicationState: 'live',
      populate: ARTICLE_LIST_RELATIONS.toString(),
      sort: ['publish_date:desc'],
    },
    {
      encodeValuesOnly: true, // prettify url
    }
  )
}

export const getArticleBySlugQuery = (articleSlug) => {
  return qs.stringify(
    {
      filters: {
        slug: {
          $eq: `${articleSlug}`,
        },
      },
      publicationState: 'live',
      populate: ARTICLE_DETAIL_RELATIONS.toString(),
    },
    {
      encodeValuesOnly: true,
    }
  )
}

export const getArticlesByCategoryQuery = (catId, page, pageSize) => {
  return qs.stringify(
    {
      filters: {
        category: {
          id: {
            $eq: catId,
          },
        },
      },
      pagination: {
        pageSize: pageSize,
        page: page,
      },
      publicationState: 'live',
      populate: ARTICLE_DETAIL_RELATIONS.toString(),
      sort: ['publish_date:desc'],
    },
    {
      encodeValuesOnly: true,
    }
  )
}

export const getArticlesByTagQuery = (tagId, page, pageSize) => {
  return qs.stringify(
    {
      filters: {
        tags: {
          id: {
            $eq: tagId,
          },
        },
      },
      pagination: {
        pageSize: pageSize,
        page: page,
      },
      publicationState: 'live',
      populate: ARTICLE_DETAIL_RELATIONS.toString(),
      sort: ['publish_date:desc'],
    },
    {
      encodeValuesOnly: true,
    }
  )
}
