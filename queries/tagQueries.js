import qs from 'qs'

export const getAllTagsQuery = () => {
  return qs.stringify(
    {
      sort: ['createdAt:desc'],
      fields: ['tag'],
      pagination: {
        pageSize: 100,
        page: 1,
      },
    },
    {
      encodeValuesOnly: true,
    }
  )
}

export const getTagByTagTextQuery = (tagText) => {
  return qs.stringify(
    {
      filters: {
        tag: {
          $eq: `${tagText}`,
        },
      },
      fields: ['tag'],
    },
    {
      encodeValuesOnly: true,
    }
  )
}
