import qs from 'qs'

export const getNavbarCategoriesQuery = () => {
  return qs.stringify(
    {
      sort: ['createdAt:asc'],
      fields: ['title', 'slug'],
      publicationState: 'live',
    },
    {
      encodeValuesOnly: true,
    }
  )
}

export const getCategoryBySlugQuery = (catSlug) => {
  return qs.stringify(
    {
      filters: {
        slug: {
          $eq: `${catSlug}`,
        },
      },
      fields: ['title', 'slug', 'description', 'show_title_in_details'],
      publicationState: 'live',
      populate: 'banner',
    },
    {
      encodeValuesOnly: true,
    }
  )
}
