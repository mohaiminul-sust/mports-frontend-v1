export default function ({ $axios, redirect }) {
  $axios.onRequest((config) => {
    console.log(`Requested url : ${config.url}`)
  })

  $axios.onError((error) => {
    if (error.response) {
      if (error.response.status)
        console.log(`APIError ${error.response.status}`)

      console.log(error.response.data.error)
    }

    // const code = parseInt(error.response && error.response.status)
    // if (code === 400) {
    //   console.log('APIError ' + error.response.status)
    //   console.log('RES' + error.response)
    // }
  })
}
