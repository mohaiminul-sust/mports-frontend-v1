export default function () {
  if (!window.console) window.console = {}

  let types = [
    'log',
    'debug',
    'warn',
    'info',
    'error',
    'dir',
    'dirxml',
    'group',
    'profile',
    'table',
  ]
  types.forEach((element) => {
    console.log(element, console[element])
  })
  types.forEach((element) => {
    console[element] = function () {}
  })
}
