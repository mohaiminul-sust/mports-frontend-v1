import Vue from 'vue'
import VueYouTubeEmbed from 'vue-youtube-embed'

const opts = {
  global: true,
  componentId: 'youtube-media',
}

Vue.use(VueYouTubeEmbed, opts)
