// for safari on iOS : for core video support over req
workbox.routing.registerRoute(
  /\.(mp4|webm)/,
  new workbox.strategies.CacheFirst({
    plugins: [new workbox.rangeRequests.RangeRequestsPlugin()],
  }),
  'GET'
)
