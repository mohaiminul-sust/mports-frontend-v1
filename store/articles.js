import {
  getArticlesQuery,
  getArticleBySlugQuery,
} from '~/queries/articleQueries'

export const state = () => ({
  loading: false,
  latestArticle: null,
  otherArticles: {
    leftColumn: [],
    rightColumn: [],
  },
  pagination: {
    page: 1,
    total: 0,
  },
})

export const mutations = {
  startLoading(state) {
    state.loading = true
  },
  stopLoading(state) {
    state.loading = false
  },
  resetArticlesData(state) {
    state.latestArticle = null
    state.otherArticles.leftColumn = []
    state.otherArticles.rightColumn = []

    state.pagination.page = 1
    state.pagination.total = 0
  },
  populateArticleColumns(state, articles) {
    articles.forEach((article, index) => {
      if (index % 2 === 0) {
        state.otherArticles.rightColumn.push(article)
      } else {
        state.otherArticles.leftColumn.push(article)
      }
    })
  },
  setArticles(state, articles) {
    if (articles.length === 0) return

    state.latestArticle = articles.shift()
    articles.forEach((article, index) => {
      if (index % 2 === 0) {
        state.otherArticles.leftColumn.push(article)
      } else {
        state.otherArticles.rightColumn.push(article)
      }
    })
  },
  setPagination(state, pagination) {
    state.pagination = { ...pagination }
  },
}

export const actions = {
  async getArticles(context, page = 1, pageSize = 10) {
    let fixedPsize = page > 1 ? pageSize : pageSize + 1

    try {
      context.commit('startLoading')
      const query = getArticlesQuery(page, fixedPsize)
      const response = await this.$axios.$get(`api/articles?${query}`)

      if (page > 1) {
        context.commit('populateArticleColumns', response.data)
      } else {
        context.commit('resetArticlesData')
        context.commit('setArticles', response.data)
      }
      context.commit('setPagination', response.meta.pagination)
    } catch (ex) {
      console.log(ex)
    } finally {
      context.commit('stopLoading')
    }
  },
  async getArticleByID(context, articleId) {
    let article = null

    try {
      context.commit('startLoading')
      const response = await this.$axios.$get(`api/articles/${articleId}`)
      article = response.data
    } catch (ex) {
      console.log(ex)
    } finally {
      context.commit('stopLoading')
    }
    return { article }
  },
  async getArticleBySlug(context, articleSlug) {
    try {
      context.commit('startLoading')
      const query = getArticleBySlugQuery(articleSlug)
      const response = await this.$axios.$get(`api/articles?${query}`)

      if (response.data.length === 0) return null

      const article = response.data[0]
      return { article }
    } catch (ex) {
      console.log(ex)
    } finally {
      context.commit('stopLoading')
    }
  },
}
