import {
  getNavbarCategoriesQuery,
  getCategoryBySlugQuery,
} from '~/queries/categoryQueries'
import { getArticlesByCategoryQuery } from '~/queries/articleQueries'

export const state = () => ({
  navbarCategories: [],
  category: {
    title: '',
    description: '',
    slug: '',
    show_title_in_details: false,
  },
  articles: {
    left: [],
    right: [],
    pagination: {
      page: 1,
      total: 0,
    },
  },
})

export const mutations = {
  setNavbarCategories(state, categories) {
    state.navbarCategories = categories
  },
  setSelectedCategory(state, category) {
    state.category = { ...category }
  },
  resetArticles(state) {
    state.articles.left = []
    state.articles.right = []

    state.articles.pagination.page = 1
    state.articles.pagination.total = 0
  },
  pushArticles(state, articles) {
    articles.forEach((element, index) => {
      if (index % 2 === 0) {
        state.articles.left.push(element)
      } else {
        state.articles.right.push(element)
      }
    })
  },
  setPagination(state, pagination) {
    state.articles.pagination = { ...pagination }
  },
}

export const actions = {
  async getNavbarCategories(context) {
    try {
      const query = getNavbarCategoriesQuery()
      const response = await this.$axios.$get(`api/categories?${query}`)

      if (response.data.length === 0) return
      context.commit('setNavbarCategories', response.data)
    } catch (ex) {
      console.log(ex)
    }
  },
  async getCategoryBySlug(context, catSlug) {
    try {
      const query = getCategoryBySlugQuery(catSlug)
      const response = await this.$axios.$get(`api/categories?${query}`)

      if (response.data.length === 0) return
      context.commit('setSelectedCategory', response.data[0])
    } catch (ex) {
      console.log(ex)
    }
  },
  async getArticlesByCategory(context, catId, page = 1, pageSize = 10) {
    try {
      const query = getArticlesByCategoryQuery(catId, page, pageSize)
      const response = await this.$axios.$get(`api/articles?${query}`)

      if (response.data.length === 0) return

      if (page === 1) {
        context.commit('resetArticles')
      }
      context.commit('pushArticles', response.data)
      context.commit('setPagination', response.meta.pagination)
    } catch (ex) {
      console.log(ex)
    }
  },
}
