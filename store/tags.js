import { getAllTagsQuery, getTagByTagTextQuery } from '~/queries/tagQueries'
import { getArticlesByTagQuery } from '~/queries/articleQueries'

export const state = () => ({
  tags: [],
  selectedTag: {
    tag: '',
  },
  articles: {
    left: [],
    right: [],
    pagination: {
      page: 1,
      total: 0,
    },
  },
})

export const mutations = {
  setTags(state, tags) {
    state.tags = tags
  },
  setSelectedTag(state, tag) {
    state.selectedTag = { ...tag }
  },
  resetArticles(state) {
    state.articles.left = []
    state.articles.right = []

    state.articles.pagination.page = 1
    state.articles.pagination.total = 0
  },
  pushArticles(state, articles) {
    articles.forEach((element, index) => {
      if (index % 2 === 0) {
        state.articles.left.push(element)
      } else {
        state.articles.right.push(element)
      }
    })
  },
  setPagination(state, pagination) {
    state.articles.pagination = { ...pagination }
  },
}

export const actions = {
  async getAllTags(context) {
    try {
      const query = getAllTagsQuery()
      const response = await this.$axios.$get(`api/tags?${query}`)

      if (response.data.length === 0) return
      context.commit('setTags', response.data)
    } catch (ex) {
      console.log(ex)
    }
  },
  async getTagByTagText(context, tagText) {
    try {
      const query = getTagByTagTextQuery(tagText)
      const response = await this.$axios.$get(`api/tags?${query}`)

      if (response.data.length === 0) return
      context.commit('setSelectedTag', response.data[0])
    } catch (ex) {
      console.log(ex)
    }
  },
  async getArticlesByTag(context, tagId, page = 1, pageSize = 10) {
    try {
      const query = getArticlesByTagQuery(tagId, page, pageSize)
      const response = await this.$axios.$get(`api/articles?${query}`)

      if (response.data.length === 0) return

      if (page === 1) {
        context.commit('resetArticles')
      }
      context.commit('pushArticles', response.data)
      context.commit('setPagination', response.meta.pagination)
    } catch (ex) {
      console.log(ex)
    }
  },
}
