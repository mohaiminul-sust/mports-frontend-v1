# Dockerfile
FROM node:11.13.0-alpine

# create destination directory
RUN mkdir -p /usr/src/mports
WORKDIR /usr/src/mports

# update and install dependency
RUN apk update && apk upgrade
RUN apk add git && apk add yarn

# copy the app, note .dockerignore
COPY . /usr/src/mports/
RUN yarn install
RUN yarn build

EXPOSE 3000

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

CMD [ "yarn", "start" ]
