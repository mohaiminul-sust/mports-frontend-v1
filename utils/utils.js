export function getRouteName(path) {
  let routeParts = path.split('/')
  return routeParts[routeParts.length - 2]
}

export function toTitleCase(text) {
  return text.toLowerCase().replace(/\b(\w)/g, (s) => s.toUpperCase())
}
