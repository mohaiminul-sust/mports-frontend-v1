export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Mports | Blog',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Our Personal Heaven',
      },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { hid: 'canonical', rel: 'canonical', href: process.env.BASE_URL },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Lato&display=swap',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@fortawesome/fontawesome-svg-core/styles.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/axios',
    '~/plugins/lazyload',
    '~/plugins/fontawesome',
    '~/plugins/markdown',
    '~/plugins/corevideo',
    '~/plugins/youtube-embed',
    '~/plugins/infinite-loader',
    '~/plugins/orientation-fixer',
    '~/plugins/pwa-sw.client',

    // prod only
    // ...(process.env.NODE_ENV === 'production'
    //   ? ['~/plugins/zero-console.js']
    //   : []),
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: ['@nuxtjs/moment'],

  moment: {
    // defaultTimezone: 'Asia/Dhaka',
    plugins: ['moment-shortformat'],
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    'nuxt-buefy',
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/google-gtag',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.API_URL,
  },

  'google-gtag': {
    id: process.env.GOOGLE_ANALYTICS_ID,
    config: {
      anonymize_ip: true, // anonymize IP
      send_page_view: true, // might be necessary to avoid duplicated page track on page reload
      // linker: {
      //   domains: ['domain.com','domain.org']
      // }
    },
    debug: false, // enable to track in dev mode
    disableAutoPageTrack: false, // disable if you don't want to track each page route with router.afterEach(...).
    // additionalAccounts: [{
    //   id: 'AW-XXXX-XX', // required if you are adding additional accounts
    //   config: {
    //     send_page_view: false // optional configurations
    //   }
    // }]
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    terser: {
      // https://github.com/terser/terser#compress-options
      terserOptions: {
        compress: {
          drop_console: true,
        },
      },
    },
  },

  // loading progress bar
  loading: {
    color: 'light-gray',
    failedColor: 'red',
    height: '5px',
    continuous: true,
  },

  // loading indicator on first load
  loadingIndicator: {
    name: 'cube-grid',
    color: 'gray',
    background: 'white',
  },

  // PWA settings
  pwa: {
    manifest: {
      name: 'Mports Journal',
      short_name: 'Mports',
      lang: 'en',
      crossorigin: 'use-credentials',
      display: 'standalone',
      useWebmanifestExtension: false,
    },
    meta: {
      theme_color: '#ffffff',
      ogHost: process.env.HOST_DOMAIN,
    },
    workbox: {
      cachingExtensions: '@/plugins/pwa-range-request.js',
      exclude: ['_redirects'],
    },
  },

  // Netlify client side 404 fallback fix
  generate: {
    fallback: true,
  },
}
